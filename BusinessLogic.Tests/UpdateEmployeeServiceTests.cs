﻿using BusinessLogic.Providers;
using BusinessLogic.Services;
using DataAccess.Entities;
using DataAccess.Repositories;
using Moq;
using Shared.Dto;

namespace BusinessLogic.Tests
{
	[TestClass]
	public class UpdateEmployeeServiceTests
	{
		private Mock<IRepository<Employee>> _repositoryMock; // Sukuria IRepository<Employee> tipo mock objektą testavimui
		private Mock<ISalaryProvider> _salaryProviderMock; // Sukuria ISalaryProvider tipo mock objektą testavimui

		private UpdateEmployeeService _service; // Testuojamas servisas


		[TestInitialize]
		public void Init()
		{
			_repositoryMock = new Mock<IRepository<Employee>>(); // Inicializuoja mock

			_repositoryMock
				.Setup(x => x.UpdateAsync(It.IsAny<Employee>())) // Nurodo, kaip elgtis, kai kviečiamas UpdateAsync
				.Returns(Task.CompletedTask); // Grąžina completed task'ą, simuliuojant sėkmingą atnaujinimą

			_repositoryMock
				.Setup(x => x.GetByIdAsync(It.IsAny<int>())) // Nurodo, kaip elgtis, kai kviečiamas GetByIdAsync
				.ReturnsAsync(new Employee()); // Grąžina naują Employee objektą, simuliuojant radimą DB

			_salaryProviderMock
				.Setup(x => x.Get(It.IsAny<string>()))// Nurodo, kaip elgtis, kai kviečiamas Get
				.Returns(111m);  // Grąžina naują atlyginima

			_service = new UpdateEmployeeService(
				_repositoryMock.Object,
				_salaryProviderMock.Object); // Sukuria UpdateEmployeeService objektą su mock priklausomybe
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))] // Nurodo, kad šis testas tikisi ArgumentNullException išimties
		public async Task CallAsyncThrowsArgumentNullExceptionWhenParameterIsNull()
		{
			// arrange
			UpdateEmployeeDto dto = null; // Sukuria scenarijų su null dto verte

			// act
			await _service.CallAsync(dto); // Bando iškviesti metodą su null parametru

			// assert
			// Tikimasi gauti išimtį, todėl assert dalies čia nereikia
		}

		[TestMethod]
		public async Task CallsEmployeeRepositoryOnce()
		{
			// arrange
			var dto = new UpdateEmployeeDto(); // Sukuria testo dto

			// act
			var result = await _service.CallAsync(dto); // Iškviečia testuojamą metodą

			// assert
			// Tikrinama, ar UpdateAsync ir GetByIdAsync metodai iš mock objekto buvo iškviesti tik po vieną kartą
			_repositoryMock.Verify(x => x.UpdateAsync(It.IsAny<Employee>()), Times.Once);
			_repositoryMock.Verify(x => x.GetByIdAsync(It.IsAny<int>()), Times.Once);

			// Patikrinama, ar nebuvo kviečiami jokie kiti mock metodai
			_repositoryMock.VerifyNoOtherCalls();
		}
	}
}
