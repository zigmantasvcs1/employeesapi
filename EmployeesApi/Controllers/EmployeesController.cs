﻿using BusinessLogic.Parameters;
using BusinessLogic.Services;
using DataAccess.Entities;
using Microsoft.AspNetCore.Mvc;
using Shared.Dto;

namespace EmployeesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IService<CreateEmployeeDto, EmployeeDto> _createEmployeeService;
        private readonly IService<ListEmployeeParameter, List<EmployeeDto>> _listEmployeeService;
        private readonly IService<UpdateEmployeeDto, EmployeeDto> _updateEmployeeService;

        public EmployeesController(
            IService<CreateEmployeeDto, EmployeeDto> createEmployeeService,
            IService<ListEmployeeParameter, List<EmployeeDto>> listEmployeeService,
            IService<UpdateEmployeeDto, EmployeeDto> updateEmployeeService)
        {
            _createEmployeeService = createEmployeeService;
            _listEmployeeService = listEmployeeService;
            _updateEmployeeService = updateEmployeeService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync(CreateEmployeeDto employee)
        {
            var result = await _createEmployeeService.CallAsync(employee);

            return StatusCode(result.Status, result);
        }

        [HttpGet]
        public async Task<IActionResult> ListAsync()
        {
            var result = await _listEmployeeService.CallAsync(new ListEmployeeParameter());

			return StatusCode(result.Status, result);
		}

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(int id, UpdateEmployeeDto employee)
        {
            if(id != employee.Id)
            {
                return BadRequest(
                    new Result<Employee>
                    {
                        Data = null,
                        DataCount = 0,
                        Errors = new List<string>() { "Id nesutampa." }
                    }
                );
            }

            var result = await _updateEmployeeService.CallAsync(employee);

			return StatusCode(result.Status, result);
		}
    }
}
