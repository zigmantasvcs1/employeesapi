using BusinessLogic.Parameters;
using BusinessLogic.Providers;
using BusinessLogic.Services;
using DataAccess;
using DataAccess.Entities;
using DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using Shared.Dto;

namespace EmployeesApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddDbContext<EmployeeDbContext>(
                options => options.UseSqlServer(
                    builder.Configuration.GetConnectionString("DefaultConnection")
                )
            );

            builder.Services.AddTransient<IRepository<Employee>, EmployeeRepository>();

            builder.Services.AddTransient<IService<CreateEmployeeDto, EmployeeDto>, CreateEmployeeService>();
            builder.Services.AddTransient<IService<ListEmployeeParameter, List<EmployeeDto>>, ListEmployeeService>();
            builder.Services.AddTransient<IService<UpdateEmployeeDto, EmployeeDto>, UpdateEmployeeService>();
            builder.Services.AddTransient<ISalaryProvider, SalaryProvider>();

			builder.Services.AddControllers();

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}
