2024-03-07 19:25:36.443 +02:00 [INF] Request starting HTTP/2 GET https://localhost:5001/swagger/index.html - null null
2024-03-07 19:25:36.875 +02:00 [INF] Request finished HTTP/2 GET https://localhost:5001/swagger/index.html - 200 null text/html;charset=utf-8 439.3268ms
2024-03-07 19:25:36.945 +02:00 [INF] Request starting HTTP/2 GET https://localhost:5001/_vs/browserLink - null null
2024-03-07 19:25:36.945 +02:00 [INF] Request starting HTTP/2 GET https://localhost:5001/_framework/aspnetcore-browser-refresh.js - null null
2024-03-07 19:25:36.992 +02:00 [INF] Request finished HTTP/2 GET https://localhost:5001/_framework/aspnetcore-browser-refresh.js - 200 13760 application/javascript; charset=utf-8 40.8958ms
2024-03-07 19:25:37.091 +02:00 [INF] Request finished HTTP/2 GET https://localhost:5001/_vs/browserLink - 200 null text/javascript; charset=UTF-8 146.8315ms
2024-03-07 19:25:37.426 +02:00 [INF] Request starting HTTP/2 GET https://localhost:5001/swagger/v1/swagger.json - null null
2024-03-07 19:25:37.664 +02:00 [INF] Request finished HTTP/2 GET https://localhost:5001/swagger/v1/swagger.json - 200 null application/json;charset=utf-8 238.3068ms
2024-03-07 19:25:53.270 +02:00 [INF] Request starting HTTP/2 POST https://localhost:5001/api/Employees - application/json 36
2024-03-07 19:25:53.285 +02:00 [INF] Executing endpoint 'EmployeesApi.Controllers.EmployeesController.CreateAsync (EmployeesApi)'
2024-03-07 19:25:53.349 +02:00 [INF] Route matched with {action = "Create", controller = "Employees"}. Executing controller action with signature System.Threading.Tasks.Task`1[Microsoft.AspNetCore.Mvc.IActionResult] CreateAsync(Shared.Dto.CreateEmployeeDto) on controller EmployeesApi.Controllers.EmployeesController (EmployeesApi).
2024-03-07 19:25:55.934 +02:00 [INF] Executed DbCommand (96ms) [Parameters=[@p0='?' (DbType = Int32), @p1='?' (Size = 4000), @p2='?' (Size = 4000)], CommandType='"Text"', CommandTimeout='30']
SET IMPLICIT_TRANSACTIONS OFF;
SET NOCOUNT ON;
INSERT INTO [Employees] ([Age], [Name], [Position])
OUTPUT INSERTED.[Id]
VALUES (@p0, @p1, @p2);
2024-03-07 19:25:56.085 +02:00 [INF] Executing OkObjectResult, writing value of type 'Shared.Dto.Result`1[[Shared.Dto.EmployeeDto, Shared, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2024-03-07 19:25:56.114 +02:00 [INF] Executed action EmployeesApi.Controllers.EmployeesController.CreateAsync (EmployeesApi) in 2751.2041ms
2024-03-07 19:25:56.118 +02:00 [INF] Executed endpoint 'EmployeesApi.Controllers.EmployeesController.CreateAsync (EmployeesApi)'
2024-03-07 19:25:56.135 +02:00 [INF] Request finished HTTP/2 POST https://localhost:5001/api/Employees - 200 null application/json; charset=utf-8 2865.074ms
