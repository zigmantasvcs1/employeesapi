﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
    public class EmployeeRepository : IRepository<Employee>
    {
        private readonly EmployeeDbContext _employeeDbContext;

        public EmployeeRepository(EmployeeDbContext employeeDbContext)
        {
            _employeeDbContext = employeeDbContext;
        }

        public string RepoName { get; } = "EmployeeRepository"; // palieku kaip pavyzdi ateiciai, kad zinotumet kad interface gali tureti ir savybe

        public async Task AddAsync(Employee entity)
        {
            _employeeDbContext.Employees.Add(entity);
            await _employeeDbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var employee = await GetByIdAsync(id);

            _employeeDbContext.Employees.Remove(employee);
            await _employeeDbContext.SaveChangesAsync();
        }

        public async Task<List<Employee>> GetAllAsync()
        {
            return await _employeeDbContext.Employees.ToListAsync();
        }

        public async Task<Employee> GetByIdAsync(int id)
        {
            var employee = await _employeeDbContext.Employees.FindAsync(id);

            if(employee == null) 
            {
                throw new KeyNotFoundException($"Employee with Id {id} not found.");
            }

            return employee;
        }

        public async Task UpdateAsync(Employee entity)
        {
            var employee = await GetByIdAsync(entity.Id);

            _employeeDbContext.Entry(employee).CurrentValues.SetValues(entity);

            await _employeeDbContext.SaveChangesAsync();
        }
    }
}
