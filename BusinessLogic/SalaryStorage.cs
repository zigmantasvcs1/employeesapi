﻿namespace BusinessLogic
{
	public static class SalaryStorage
	{
		public static Dictionary<string, decimal> Salaries { get; } = new Dictionary<string, decimal>
		{
			{ "Trial", 1000m },
			{ "Developer", 999m },
			{ "Product manager", 5000m },
			{ "Data analyst", 1000m }
		};
	}
}
