﻿using BusinessLogic.Parameters;
using BusinessLogic.Providers;
using DataAccess.Entities;
using DataAccess.Repositories;
using Shared.Dto;

namespace BusinessLogic.Services
{
    public class UpdateEmployeeService : IService<UpdateEmployeeDto, EmployeeDto>
    {
        private readonly IRepository<Employee> _employeeRepository; // Deklaruoja priklausomybę nuo IRepository<Employee>
		private readonly ISalaryProvider _salaryProvider; // privatus laukas salaryProvider objektui saugoti

		public UpdateEmployeeService(
            IRepository<Employee> employeeRepository, 
            ISalaryProvider salaryProvider)
		{
			_employeeRepository = employeeRepository; // Inicializuoja priklausomybę konstruktoriuje
			_salaryProvider = salaryProvider;
		}

		public async Task<Result<EmployeeDto>> CallAsync(UpdateEmployeeDto dto)
        {
            if (dto == null) // Tikrina, ar perduotas parametras nėra null
            {
                throw new ArgumentNullException(nameof(dto)); // Meta išimtį, jei parametras yra null
            }

            // išsitraukiam egzistuojantį employee, kad padengtume trūkstamas savybes, šiuo atveju Name
            var existingEmployee = await _employeeRepository.GetByIdAsync(dto.Id);

            // Konvertuoti EmployeeDto į Employee entity
            var entityToUpdate = new Employee()
            {
                Id = dto.Id, // Nustato ID iš parametro
                Age = dto.Age, // Nustato amžių iš parametro
                Position = dto.Position, // Nustato poziciją iš parametro
                Name = existingEmployee.Name // šitą naudojame iš duomenų bazės nes DTO nepadengia, o jei paliksime null, bus pakeista reikšmė į null
            };

            // Iškviečia repositoriją atnaujinti entity ir laukia rezultato
            await _employeeRepository.UpdateAsync(entityToUpdate);

            // Konvertuoja entity į DTO
            var employeeDto = new EmployeeDto
            {
                Id = entityToUpdate.Id, // Nustato ID iš atnaujinto entity
                Name = entityToUpdate.Name,
                Age = entityToUpdate.Age, // Nustato amžių iš atnaujinto entity
                Position = entityToUpdate.Position, // Nustato poziciją iš atnaujinto entity
                Salary = _salaryProvider.Get(entityToUpdate.Position)
			};

            // Grąžina rezultatą su atnaujintu DTO
            return new Result<EmployeeDto>()
            {
                Data = employeeDto, // Pridedama atnaujinta DTO duomenis
                DataCount = 1, // Nustato, kad grąžinamas vienas įrašas
                Errors = new List<string>() // Sukuria tuščią klaidų sąrašą
            };
        }
    }
}
