﻿using BusinessLogic.Parameters;
using BusinessLogic.Providers;
using DataAccess.Entities; // Naudoja duomenų prieigos sluoksnio entičių apibrėžimus
using DataAccess.Repositories; // Naudoja duomenų prieigos sluoksnio repositorijų apibrėžimus
using Shared.Dto; // Naudoja bendrai naudojamus DTO apibrėžimus

namespace BusinessLogic.Services
{
    // Apibrėžia ListEmployeeService klasę, kuri yra paslauga darbuotojų sąrašui gauti
    public class ListEmployeeService : IService<ListEmployeeParameter, List<EmployeeDto>>
    {
        private readonly IRepository<Employee> _repository; // Privatus laukas repositorijai saugoti
        private readonly ISalaryProvider _salaryProvider; // privatus laukas salaryProvider objektui saugoti

		// Konstruktorius su priklausomybės injekcija IRepository<Employee> tipo objekto
		public ListEmployeeService(
            IRepository<Employee> repository, 
            ISalaryProvider salaryProvider)
		{
			_repository = repository; // Priklausomybės priskyrimas
			_salaryProvider = salaryProvider;
		}

		// Metodas, skirtas iškviesti paslaugą asinchroniškai
		public async Task<Result<List<EmployeeDto>>> CallAsync(ListEmployeeParameter parameter)
        {
            var employees = await _repository.GetAllAsync(); // Gaus visus darbuotojus iš repositorijos asinchroniškai

            var limitedEmployees = employees.Take(parameter.Limit); // Ribos darbuotojų skaičių pagal perduotą limitą

            List<EmployeeDto> employeeDtos = new List<EmployeeDto>(); // Sukurs naują sąrašą darbuotojų DTO

            foreach (var item in limitedEmployees) // Iteruos per ribotą darbuotojų sąrašą
            {
                employeeDtos.Add( // Pridės kiekvieną darbuotoją į DTO sąrašą
                    new EmployeeDto
                    {
                        Id = item.Id, // Nustatys darbuotojo ID
                        Name = item.Name, // Nustatys darbuotojo vardą
                        Position = item.Position, // Nustatys darbuotojo poziciją
                        Age = item.Age, // Nustatys darbuotojo amžių
                        Salary = _salaryProvider.Get(item.Position)
					}
                );
            }

            // Grąžins rezultatą su darbuotojų DTO sąrašu
            return new Result<List<EmployeeDto>>
            {
                Data = employeeDtos, // Priskiria duomenis
                DataCount = employeeDtos.Count, // Nustato duomenų kiekį
                Errors = new List<string>(), // Sukuria tuščią klaidų sąrašą
                Status = 200
            };
        }
    }
}
