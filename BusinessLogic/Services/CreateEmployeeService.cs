﻿using BusinessLogic.Providers;
using DataAccess.Entities; // Naudoja duomenų prieigos sluoksnio entičių apibrėžimus.
using DataAccess.Repositories; // Naudoja duomenų prieigos sluoksnio repositorijų apibrėžimus.
using Shared.Dto; // Naudoja bendrai naudojamus DTO apibrėžimus.

namespace BusinessLogic.Services
{
	// Apibrėžia CreateEmployeeService klasę, kuri implementuoja IService interfeisą naujo darbuotojo sukūrimui.
	public class CreateEmployeeService : IService<CreateEmployeeDto, EmployeeDto>
	{
		private IRepository<Employee> _repository; // Privatus laukas saugoti repositorijai.
		private readonly ISalaryProvider _salaryProvider; // privatus laukas salaryProvider objektui saugoti

		// Konstruktorius su priklausomybės injekcija IRepository<Employee> tipo objekto.
		public CreateEmployeeService(
			IRepository<Employee> repository, 
			ISalaryProvider salaryProvider)
		{
			_repository = repository; // Priklausomybės priskyrimas.
			_salaryProvider = salaryProvider;
		}

		// Asinchroninis metodas, kuris įgyvendina paslaugą pagal IService interfeisą.
		public async Task<Result<EmployeeDto>> CallAsync(CreateEmployeeDto dto)
		{
			// Sukuria naują Employee objektą pagal perduotą parametrą.
			var employee = new Employee()
			{
				Name = dto.Name, // Nustato vardą pagal parametrą.
				Age = dto.Age, // Nustato amžių pagal parametrą.
				Position = "Trial" // Priskiria poziciją kaip "Trial".
			};

			await _repository.AddAsync(employee); // Prideda naują darbuotoją į duomenų bazę asinchroniškai.

			// Sukuria EmployeeDto objektą su naujo darbuotojo duomenimis.
			var newEmployeeDto = new EmployeeDto
			{
				Id = employee.Id, // Nustato ID pagal sukurtą darbuotoją.
				Name = employee.Name, // Nustato vardą pagal sukurtą darbuotoją.
				Age = employee.Age, // Nustato amžių pagal sukurtą darbuotoją.
				Position = employee.Position, // Nustato poziciją pagal sukurtą darbuotoją.
				Salary = _salaryProvider.Get(employee.Position) // gauti atlyginimą pagal pareigas
			};

			// Grąžina rezultatą su naujuoju darbuotojo DTO.
			return new Result<EmployeeDto>
			{
				Data = newEmployeeDto, // Priskiria duomenis.
				DataCount = 1, // Nustato, kad grąžinamas vienas įrašas.
				Errors = new List<string>() // Sukuria tuščią klaidų sąrašą.
			};
		}
	}
}
