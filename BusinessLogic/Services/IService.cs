﻿using Shared.Dto; // Įtraukia bendrai naudojamų duomenų perdavimo objektų (DTO) apibrėžimus

namespace BusinessLogic.Services
{
    // Apibrėžia IService sąsają su dviem tipais: TParameter ir TData
    public interface IService<TParameter, TData> where TData : class
    {
        // Apibrėžia metodą CallAsync, kuris turi būti įgyvendintas visose klasėse, kurios implementuoja šią sąsają.
        // Šis metodas priima parametrą tipo TParameter ir grąžina Task, kuris rezultate turės Result<TData> tipo objektą.
        Task<Result<TData>> CallAsync(TParameter parameter);
    }
}
