﻿namespace BusinessLogic.Providers
{
	public interface ISalaryProvider
	{
		decimal Get(string position);
	}
}
