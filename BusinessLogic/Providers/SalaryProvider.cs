﻿namespace BusinessLogic.Providers
{
	public class SalaryProvider : ISalaryProvider
	{
		private Dictionary<string, decimal> salaries = new Dictionary<string, decimal>
		{
			{ "Trial", 1000m },
			{ "Developer", 999m },
			{ "Product manager", 5000m },
			{ "Data analyst", 1000m }
		};

		public decimal Get(string position)
		{
			SalaryStorage.Salaries.TryGetValue(position, out var salary);

			return salary;
		}
	}
}
