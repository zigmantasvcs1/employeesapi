﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Parameters
{
    public class ListEmployeeParameter
    {
        public ListEmployeeParameter(int limit = 10)
        {
            Limit = limit;
        }

        public int Limit { get; }
    }
}
