﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Dto
{
    public class Result<T> where T : class
    {
        public T Data { get; set; }
        public int DataCount { get; set; }
        public List<string> Errors { get; set; } = new List<string>();
        public int Status { get; set; } = 200;
    }
}
