﻿namespace Shared.Dto
{
    public class UpdateEmployeeDto
    {
        public int Id { get; set; }
        public int Age { get; set; }
        public string Position { get; set; }
    }
}
