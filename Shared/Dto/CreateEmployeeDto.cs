﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Dto
{
    public class CreateEmployeeDto
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
